package main;

import captor.Captor;
import captor.strategies.AtomicDiffusion;
import captor.strategies.SequentialDiffusion;
import captor.strategies.TimeDiffusion;
import javafx.fxml.FXML;

/**
 * Controller
 * The javafx controller
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class Controller {

    private Captor captor;

    /**
     * setCaptor
     * Set a captor to the controller
     *
     * @param captor Captor: the captor
     * @throw NullPointerException if captor is null
     */
    public void setCaptor(Captor captor){
        if(captor == null) throw new NullPointerException("captor cannot be null");

        this.captor = captor;
    }

    /**
     * start
     * Call to the captor start method
     *
     * @throw NullPointerException if captor is null
     */
    @FXML
    public void start(){
        if(this.captor == null) throw new NullPointerException("captor must be set");

        this.captor.start();
    }

    /**
     * stop
     * Call to the captor stop method
     *
     * @throw NullPointerException if captor is null
     */
    @FXML
    public void stop(){
        if(this.captor == null) throw new NullPointerException("captor must be set");

        this.captor.stop();
    }

    /**
     * sequential
     * Call to the captor setAlgo method with SequentialDiffusion parameter
     *
     * @throw NullPointerException if captor is null
     */
    @FXML
    public void sequential(){
        if(this.captor == null) throw new NullPointerException("captor must be set");

        this.captor.setAlgo(new SequentialDiffusion());
    }

    /**
     * atomic
     * Call to the captor setAlgo method with AtomicDiffusion parameter
     *
     * @throw NullPointerException if captor is null
     */
    @FXML
    public void atomic(){
        if(this.captor == null) throw new NullPointerException("captor must be set");

        this.captor.setAlgo(new AtomicDiffusion());
    }

    /**
     * time
     * Call to the captor setAlgo method with TimeDiffusion parameter
     *
     * @throw NullPointerException if captor is null
     */
    @FXML
    public void time(){
        if(this.captor == null) throw new NullPointerException("captor must be set");

        this.captor.setAlgo(new TimeDiffusion());
    }
}

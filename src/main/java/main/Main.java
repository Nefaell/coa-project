package main;

import digitalDisplay.DigitalDisplay;
import digitalDisplay.DigitalDisplayImpl;
import captor.Captor;
import captor.CaptorImpl;
import captor.strategies.SequentialDiffusion;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import proxy.Canal;
import scheduler.Scheduler;

/**
 * Main
 * The main class of the application
 * Configure the application and the view
 *
 * @see Application
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class Main extends Application {

    //Number of digital display to add to view
    private static final int NB_DIGITAL_DISPLAY = 3;

    /**
     * start
     * Method called by JavaFX on starting the application
     *
     * @param primaryStage the main stage of the GUI.
     * @throws Exception If something wrong happen.
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("COA project");
        Scene scene = new Scene(root, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

        Captor captor = new CaptorImpl(new SequentialDiffusion(), (Canal.DELAY_MAX)/2);

        Controller controller = loader.getController();
        controller.setCaptor(captor);

        GridPane gridPane = (GridPane) scene.lookup("#gridPane");
        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(50);
        col.setHalignment(HPos.CENTER);
        gridPane.getColumnConstraints().addAll(col,col);

        for(int i = 0; i < Main.NB_DIGITAL_DISPLAY; i++){
            DigitalDisplay digitalDisplay = new DigitalDisplayImpl();
            Canal canal = new Canal(digitalDisplay, captor);
            gridPane.add((Text) digitalDisplay, i%2, i/2);
            captor.addDigitalDisplay(canal);
        }

        ToggleGroup group = new ToggleGroup();
        RadioButton sequential = (RadioButton) scene.lookup("#sequential");
        sequential.setToggleGroup(group);
        RadioButton time = (RadioButton) scene.lookup("#time");
        time.setToggleGroup(group);
        RadioButton atomic = (RadioButton) scene.lookup("#atomic");
        atomic.setToggleGroup(group);

        primaryStage.setOnCloseRequest(we -> {
            captor.stop();
            Scheduler.scheduler.shutdown();
        });

    }


    public static void main(String... args) {
        Application.launch(args);
    }
}
package digitalDisplay;

import captor.CaptorAsync;

/**
 * DigitalDisplay
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public interface DigitalDisplay {

    /**
     * update
     * update the text display on digital display
     *
     * @param subject CaptorAsync: the captor which notify an update
     */
    void update(CaptorAsync subject);
}

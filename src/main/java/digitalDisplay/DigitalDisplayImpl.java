package digitalDisplay;

import captor.CaptorAsync;
import captor.strategies.TimeStamp;
import javafx.application.Platform;
import javafx.scene.text.Text;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * DigitalDisplayImpl
 *
 * @see Text
 * @see DigitalDisplay
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class DigitalDisplayImpl extends Text implements DigitalDisplay {

    /**
     * Constructor of DigitalDisplayImpl
     * Set style class and set initial text
     */
    public DigitalDisplayImpl() {
        super();
        getStyleClass().add("digitalDisplay");
        setText("-");
    }

    /**
     * update
     * update the text display on digital display
     *
     * @param subject CaptorAsync: the captor which notify an update
     */
    @Override
    public void update(CaptorAsync subject) {
        Future f = subject.getValue(this);

        try {
            TimeStamp timeStamp = (TimeStamp)f.get();
            Integer value = timeStamp.getValue();
            Integer time = timeStamp.getTimeStamp();

            if(time == null)
                Platform.runLater ( () -> setText(String.valueOf(value)));
            else
                Platform.runLater( () -> setText(time + " - " + value));
        }
        catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}

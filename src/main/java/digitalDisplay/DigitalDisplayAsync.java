package digitalDisplay;

import captor.Captor;

import java.util.concurrent.Future;

/**
 * DigitalDisplayAsync
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public interface DigitalDisplayAsync {

    /**
     * update
     * Schedule new task to update digital display
     *
     * @param captor Captor: the captor which notify an update
     * @return Future
     */
    Future update(Captor captor);
}

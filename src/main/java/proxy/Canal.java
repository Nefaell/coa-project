package proxy;

import digitalDisplay.DigitalDisplay;
import digitalDisplay.DigitalDisplayAsync;
import captor.Captor;
import captor.CaptorAsync;
import scheduler.Scheduler;

import java.util.Random;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * Canal
 *
 * @see DigitalDisplayAsync
 * @see CaptorAsync
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class Canal implements DigitalDisplayAsync, CaptorAsync {

    public static final int DELAY_MAX = 1000;

    private final DigitalDisplay digitalDisplay;
    private final Captor captor;

    /**
     * Constructor of Canal
     * Initialize attribute
     *
     * @throws NullPointerException if one of the parameter is null
     */
    public Canal(DigitalDisplay digitalDisplay, Captor captor) {
        super();
        if (digitalDisplay == null) throw new NullPointerException("digitalDisplay cannot be null");
        if (captor == null) throw new NullPointerException("captor cannot be null");

        this.digitalDisplay = digitalDisplay;
        this.captor = captor;
    }

    /**
     * getValue
     * Method getValue ASYNC
     * Schedule task to get the captor value
     *
     * @param digitalDisplay DigitalDisplay: the digital display which call to get captor value
     * @return Future: future which contains the captor value
     */
    @Override
    public Future getValue(DigitalDisplay digitalDisplay) {
        Random r = new Random();
        int delay = r.nextInt(DELAY_MAX);
        return Scheduler.scheduler.schedule(() -> this.captor.getValue(this), delay, TimeUnit.MILLISECONDS);
    }

    /**
     * update
     * Method update ASYNC
     * Schedule task to update the digital display text
     *
     * @param captor Captor: the captor which calls to notify an update
     * @return Future: future which contains void
     */
    @Override
    public Future update(Captor captor) {
        Random r = new Random();
        int delay = r.nextInt(DELAY_MAX);
        return Scheduler.scheduler.schedule(() -> this.digitalDisplay.update(this), delay, TimeUnit.MILLISECONDS);
    }
}

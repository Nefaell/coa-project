package exception;

/**
 * Custom exception to handle when values of tempo, measure, beats are not correct.
 * Extends {@link Exception}.
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class ValueOutOfBoundException extends Exception {

    /**
     * Construct an {@link Exception} with specific message.
     * @param message the message of the exception.
     */
    public ValueOutOfBoundException(String message) {
        super(message);
    }
}

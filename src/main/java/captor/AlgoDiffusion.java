package captor;

import digitalDisplay.DigitalDisplayAsync;
import java.util.List;

/**
 * AlgoDiffusion
 * The strategy from the strategy design pattern
 * Allows CaptorImpl to use different kinds of algorithms to updates the displayers
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public interface AlgoDiffusion {
    /**
     * Indicates to the algorithm the displayers to updates and the captor to take the value from
     *
     * @param digitalDisplayList ArrayList<DigitalDisplayAsync>: The different displayers
     * @param captor Captor: The captor which holds the current value
     */
    void configure(List<DigitalDisplayAsync> digitalDisplayList, Captor captor);

    /**
     * Execute the algorithm, updates the different displayers with the current value according to a custom algorithm
     *
     */
    void execute();

    /**
     * Used in some algorithms, called when a displayer asked for the latest value of the captor
     *
     * @param digitalDisplayAsync DigitalDisplayAsync; The displayer that called the getValue() method from captor
     */
    void call(DigitalDisplayAsync digitalDisplayAsync);
}
package captor.strategies;

/**
 * TimeStamp
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class TimeStamp {

    private final int value;
    private final Integer timeStamp;

    public TimeStamp(int value, Integer timeStamp) {
        super();
        this.value = value;
        this.timeStamp = timeStamp;
    }

    public int getValue() {
        return this.value;
    }

    public Integer getTimeStamp(){
        return this.timeStamp;
    }
}

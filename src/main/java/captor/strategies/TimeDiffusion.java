package captor.strategies;

import digitalDisplay.DigitalDisplayAsync;
import captor.AlgoDiffusion;
import captor.Captor;
import java.util.List;

/**
 * TimeDiffusion
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class TimeDiffusion implements AlgoDiffusion {
    private List<DigitalDisplayAsync> digitalDisplayList;
    private Captor captor;

    /**
     * configure
     *
     * @param digitalDisplayList ArrayList<DigitalDisplayAsync>
     * @param captor Captor
     */
    @Override
    public void configure(List<DigitalDisplayAsync> digitalDisplayList, Captor captor) {
        this.digitalDisplayList = digitalDisplayList;
        this.captor = captor;
        captor.setTimeStamp(true);
    }

    /**
     * execute
     *
     */
    @Override
    public void execute() {
        this.captor.unlock();
        this.digitalDisplayList.forEach(a ->a.update(this.captor));
    }

    /**
     * call
     *
     * @param digitalDisplayAsync DigitalDisplayAsync
     */
    @Override
    public void call(DigitalDisplayAsync digitalDisplayAsync) {

    }
}

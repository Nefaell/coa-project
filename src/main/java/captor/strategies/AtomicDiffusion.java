package captor.strategies;

import digitalDisplay.DigitalDisplayAsync;
import captor.AlgoDiffusion;
import captor.Captor;
import java.util.List;

/**
 * An algorithm that waits for all the displayers to be updated before allowing the captor to change its value
 *
 * @see AlgoDiffusion
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class AtomicDiffusion implements AlgoDiffusion {
    private List<DigitalDisplayAsync> digitalDisplayList;
    private Captor captor;

    /**
     * configure
     *
     * @param digitalDisplayList List<DigitalDisplayAsync>
     * @param captor Captor
     */
    @Override
    public void configure(List<DigitalDisplayAsync> digitalDisplayList, Captor captor) {
        if(digitalDisplayList == null) throw new NullPointerException("digitalDisplayList cannot be null");
        if(captor == null) throw new NullPointerException("captor cannot be null");

        this.digitalDisplayList = digitalDisplayList;
        this.captor = captor;
        captor.setTimeStamp(false);
    }

    /**
     * execute
     *
     */
    @Override
    public void execute() {
        this.captor.lock();
        this.digitalDisplayList.forEach(a ->a.update(this.captor));
    }

    /**
     * call
     *
     * @param digitalDisplayAsync DigitalDisplayAsync
     */
    @Override
    public void call(DigitalDisplayAsync digitalDisplayAsync) {
        this.digitalDisplayList.remove(digitalDisplayAsync);
        if(this.digitalDisplayList.isEmpty()){
            this.captor.unlock();
        }
    }
}

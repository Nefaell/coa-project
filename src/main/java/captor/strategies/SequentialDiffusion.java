package captor.strategies;

import digitalDisplay.DigitalDisplayAsync;
import captor.AlgoDiffusion;
import captor.Captor;
import java.util.List;

/**
 * A algorithm that give total freedom to the displayers and the captor:
 * The displayers can call getValue whenever they want and the captor updates its value without waiting for the displayers to be updated
 *
 * @see AlgoDiffusion
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class SequentialDiffusion implements AlgoDiffusion {
    private List<DigitalDisplayAsync> digitalDisplayList;
    private Captor captor;

    /**
     * @param digitalDisplayList ArrayList<DigitalDisplayAsync>
     * @param captor Captor
     */
    @Override
    public void configure(List<DigitalDisplayAsync> digitalDisplayList, Captor captor) {
        if(digitalDisplayList == null) throw new NullPointerException("digitalDisplayList cannot be null");
        if(captor == null) throw new NullPointerException("captor cannot be null");

        this.digitalDisplayList = digitalDisplayList;
        this.captor = captor;
        captor.setTimeStamp(false);
    }

    /**
     * execute
     *
     */
    @Override
    public void execute() {
        this.captor.unlock();
        this.digitalDisplayList.forEach(a ->a.update(this.captor));
    }

    /**
     * call
     *
     * @param digitalDisplayAsync DigitalDisplayAsync
     */
    @Override
    public void call(DigitalDisplayAsync digitalDisplayAsync) {

    }
}

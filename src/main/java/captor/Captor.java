package captor;

import captor.strategies.TimeStamp;
import digitalDisplay.DigitalDisplayAsync;

/**
 * Captor
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public interface Captor {
    /**
     * Set the diffusion algorithm, strategy design pattern
     * @param algo: the algorithm that will manage the updates of the displayers
     */
    void setAlgo(AlgoDiffusion algo);

    /**
     * Add a digitalDisplay to the list of the displayers to manage
     * @param digitalDisplay: The new displayer to be updated
     */
    void addDigitalDisplay(DigitalDisplayAsync digitalDisplay);

    /**
     * Remove a displayers from the list of diplayers to manage
     * @param digitalDisplay: The displayer to be removed
     */
    void removeDigitalDisplay(DigitalDisplayAsync digitalDisplay);

    /**
     * Periodic method that increments the value of the captor and notifies the displayers using the algorithm
     */
    void tick();

    /**
     * Starts the periodic call of the tick() method
     */
    void start();

    /**
     * Stops the periodic call of the tick() method
     */
    void stop();

    /**
     * Calling this method blocks the tick() method from being executed
     */
    void lock();

    /**
     * Calling this method allows the tick method to be executed
     */
    void unlock();

    /**
     * Set a timeStamp to be returned or not
     */
    void setTimeStamp(boolean b);

    /**
     * Returns the current value of the captor in the form of an TimeStamp
     * The reference is used in algorithms
     * @param ref: the calling displayer
     * @return the current value of the captor
     */
    TimeStamp getValue(DigitalDisplayAsync ref);
}

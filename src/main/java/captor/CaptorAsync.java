package captor;

import digitalDisplay.DigitalDisplay;

import java.util.concurrent.Future;

/**
 * CaptorAsync
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public interface CaptorAsync {

    /**
     * getValue
     * @param digitalDisplay DigitalDisplay: digital display which call to get the value
     * @return Future
     */
    Future getValue(DigitalDisplay digitalDisplay);
}

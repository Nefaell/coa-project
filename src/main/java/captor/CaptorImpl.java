package captor;

import captor.strategies.TimeStamp;
import digitalDisplay.DigitalDisplayAsync;
import exception.ValueOutOfBoundException;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * CaptorImpl
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class CaptorImpl implements Captor {

    private int value;
    private final long period;
    private boolean lock;
    private boolean time;


    private AlgoDiffusion algo;
    private final ArrayList<DigitalDisplayAsync> digitalDisplayList;
    private Timer clock;

    public CaptorImpl(AlgoDiffusion algo, long period) throws ValueOutOfBoundException {
        super();
        if (algo == null) throw new NullPointerException("cannot set to a null algo");
        if (period <= 0) throw new ValueOutOfBoundException("period must be positive");
        this.algo = algo;
        this.digitalDisplayList = new ArrayList<>();
        this.clock = new Timer();
        this.value = 0;
        this.period = period;
        this.lock = false;
    }

    @Override
    public void setAlgo(AlgoDiffusion algo){
        if(algo == null) throw new NullPointerException("cannot set to a null algo");
        this.algo = algo;
        unlock();
    }

    @Override
    public void addDigitalDisplay(DigitalDisplayAsync digitalDisplay) {
        if(digitalDisplay == null) throw new NullPointerException("cannot add a null digitalDisplay");
        this.digitalDisplayList.add(digitalDisplay);
    }

    @Override
    public void removeDigitalDisplay(DigitalDisplayAsync digitalDisplay) {
        if(digitalDisplay == null) throw new NullPointerException("cannot remove a null digitalDisplay");
        this.digitalDisplayList.remove(digitalDisplay);
    }

    @Override
    public void tick() {
        if(!this.lock) {
            this.value++;
            this.algo.configure((ArrayList<DigitalDisplayAsync>) this.digitalDisplayList.clone(), this);
            this.algo.execute();
        }
    }

    @Override
    public void start() {
        this.clock.cancel();
        this.clock.purge();

        this.clock = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                tick();
            }
        };
        this.clock.scheduleAtFixedRate(timerTask, this.period, this.period);
    }

    @Override
    public void stop() {
        this.clock.cancel();
        this.clock.purge();
    }

    @Override
    public void lock() {
        this.lock = true;
    }

    @Override
    public void unlock() {
        this.lock = false;
    }

    @Override
    public void setTimeStamp(boolean b) {
        this.time = b;
    }

    @Override
    public TimeStamp getValue(DigitalDisplayAsync ref) {
        if(ref == null) throw new NullPointerException("digitalDisplay cannot be null");
        this.algo.call(ref);

        return new TimeStamp(this.value, !this.time ? null : this.value);

    }
}
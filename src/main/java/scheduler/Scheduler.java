package scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Scheduler
 *
 * @see ScheduledExecutorService
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public final class Scheduler {

    public static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(10);

}

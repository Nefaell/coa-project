package main;

import captor.Captor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * ControllerTest
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class ControllerTest {

    @InjectMocks
    private Controller controller;

    @Mock
    private Captor captor;

    @Before
    public void setUp() throws Exception {
        controller = spy(new Controller());
        initMocks(this);
    }

    @Test
    public void setCaptor() throws Exception {
        this.controller.setCaptor(mock(Captor.class));
    }

    @Test
    public void start() throws Exception {
        this.controller.start();

        verify(this.captor).start();
    }

    @Test
    public void stop() throws Exception {
        this.controller.stop();

        verify(this.captor).stop();
    }

    @Test
    public void sequential() throws Exception {
        this.controller.sequential();

        verify(this.captor).setAlgo(any());
    }

    @Test
    public void atomic() throws Exception {
        this.controller.atomic();

        verify(this.captor).setAlgo(any());
    }

    @Test
    public void time() throws Exception {
        this.controller.time();

        verify(this.captor).setAlgo(any());
    }

}
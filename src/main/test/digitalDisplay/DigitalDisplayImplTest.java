package digitalDisplay;

import captor.CaptorAsync;
import captor.strategies.TimeStamp;
import javafx.embed.swing.JFXPanel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import javax.swing.SwingUtilities;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * DigitalDisplayImplTest
 * Created by quentin on 20/01/17.
 */
public class DigitalDisplayImplTest {

    @InjectMocks
    private DigitalDisplayImpl digitalDisplay;

    @BeforeClass
    public static void initToolkit() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        SwingUtilities.invokeLater(() -> {
            new JFXPanel();
            latch.countDown();
        });

        if (!latch.await(5L, TimeUnit.SECONDS))
            throw new ExceptionInInitializerError();
    }

    @Before
    public void setUp() throws Exception {
        this.digitalDisplay = Mockito.spy(new DigitalDisplayImpl());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void update() throws Exception {
        CaptorAsync captor = Mockito.mock(CaptorAsync.class);
        Future future = Mockito.mock(Future.class);
        TimeStamp value = new TimeStamp(8, null);

        Mockito.when(captor.getValue(digitalDisplay)).thenReturn(future);
        Mockito.when(future.get()).thenReturn(value);

        digitalDisplay.update(captor);

        Thread.sleep(100);

        Assert.assertEquals(digitalDisplay.getText(), String.valueOf(((TimeStamp)future.get()).getValue()));


        value = new TimeStamp(8, 6);

        Mockito.when(captor.getValue(digitalDisplay)).thenReturn(future);
        Mockito.when(future.get()).thenReturn(value);

        digitalDisplay.update(captor);

        Thread.sleep(100);

        TimeStamp f = (TimeStamp)future.get();
        Assert.assertEquals(digitalDisplay.getText(), String.valueOf(f.getTimeStamp() + " - " + f.getValue()));


    }

}
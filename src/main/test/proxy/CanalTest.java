package proxy;

import digitalDisplay.DigitalDisplay;
import captor.Captor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * CanalTest
 * Created by quentin on 21/01/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CanalTest {

    private static final int DELAY_MIN = 10;
    private static final int DELAY_MAX = 100;
    private static final int EPSILON = 3;

    private Canal canal;
    private DigitalDisplay digitalDisplay;
    private Captor captor;


    @Before
    public void setUp() throws Exception {
        digitalDisplay = Mockito.mock(DigitalDisplay.class);
        captor = Mockito.mock(Captor.class);
        canal = Mockito.spy(new Canal(digitalDisplay, captor));
    }

    @Test(expected = NullPointerException.class)
    public void canalWithNullDigitalDisplay() throws Exception {
        new Canal(null, Mockito.mock(Captor.class));
    }

    @Test(expected = NullPointerException.class)
    public void canalWithNullCaptor() throws Exception {
        new Canal(Mockito.mock(DigitalDisplay.class), null);
    }

    @Test
    public void getValue() throws Exception {
        canal.getValue(digitalDisplay);

        Thread.sleep(CanalTest.DELAY_MIN - CanalTest.EPSILON);
        Mockito.verify(captor, Mockito.times(0)).getValue(canal);

        Thread.sleep(CanalTest.DELAY_MAX - CanalTest.DELAY_MIN + 2 * CanalTest.EPSILON);
        Mockito.verify(captor, Mockito.times(1)).getValue(canal);
    }

    @Test
    public void update() throws Exception {
        canal.update(captor);

        Thread.sleep(CanalTest.DELAY_MIN - CanalTest.EPSILON);
        Mockito.verify(digitalDisplay, Mockito.times(0)).update(canal);

        Thread.sleep(CanalTest.DELAY_MAX - CanalTest.DELAY_MIN + 2 * CanalTest.EPSILON);
        Mockito.verify(digitalDisplay, Mockito.times(1)).update(canal);
    }

}
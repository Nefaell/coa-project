package captor.strategies;

import captor.Captor;
import digitalDisplay.DigitalDisplayAsync;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.List;

/**
 * AtomicDiffusionTest
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class AtomicDiffusionTest {

    @InjectMocks
    private AtomicDiffusion algo;

    @Mock
    private List<DigitalDisplayAsync> digitalDisplayList;

    @Mock
    private Captor captor;

    @Before
    public void setUp() throws Exception {
        algo = Mockito.spy(new AtomicDiffusion());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void configure() throws Exception {
        algo.configure(digitalDisplayList, captor);
    }

    @Test(expected = NullPointerException.class)
    public void configureWithNullList() throws Exception {
        algo.configure(null, captor);
    }

    @Test(expected = NullPointerException.class)
    public void configureWithNullCaptor() throws Exception {
        algo.configure(digitalDisplayList, null);
    }

    @Test
    public void execute() throws Exception {
        algo.configure(digitalDisplayList, captor);

        algo.execute();
        Mockito.verify(captor, Mockito.times(1)).lock();

        algo.execute();
        Mockito.verify(captor, Mockito.times(2)).lock();
    }

    @Test(expected = NullPointerException.class)
    public void executeBeforeConfigure() throws Exception {
        AtomicDiffusion atomicDiffusion = new AtomicDiffusion();
        atomicDiffusion.execute();
    }

    @Test
    public void call() throws Exception {
        Mockito.when(digitalDisplayList.isEmpty()).thenReturn(false, true);

        algo.configure(digitalDisplayList, captor);
        algo.call(Mockito.mock(DigitalDisplayAsync.class));

        Mockito.verify(captor, Mockito.times(0)).unlock();

        algo.call(Mockito.mock(DigitalDisplayAsync.class));

        Mockito.verify(captor, Mockito.times(1)).unlock();
    }

    @Test(expected = NullPointerException.class)
    public void callWithNullParameter() throws Exception {
        AtomicDiffusion atomicDiffusion = new AtomicDiffusion();
        atomicDiffusion.call(null);
    }

    @Test(expected = RuntimeException.class)
    public void callBeforeConfigure() throws Exception {
        AtomicDiffusion atomicDiffusion = new AtomicDiffusion();
        atomicDiffusion.call(Mockito.mock(DigitalDisplayAsync.class));
    }

}
package captor.strategies;

import captor.Captor;
import digitalDisplay.DigitalDisplayAsync;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.List;

/**
 * AtomicDiffusionTest
 *
 * @author CADORET Ronan, GUILLOU Quentin
 * @version 1.0
 */
public class SequentialDiffusionTest {

    @InjectMocks
    private SequentialDiffusion algo;

    @Mock
    private List<DigitalDisplayAsync> digitalDisplayList;

    @Mock
    private Captor captor;

    @Before
    public void setUp() throws Exception {
        algo = Mockito.spy(new SequentialDiffusion());
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void configure() throws Exception {
        algo.configure(digitalDisplayList, captor);
    }

    @Test(expected = NullPointerException.class)
    public void configureWithNullList() throws Exception {
        algo.configure(null, captor);
    }

    @Test(expected = NullPointerException.class)
    public void configureWithNullCaptor() throws Exception {
        algo.configure(digitalDisplayList, null);
    }

    @Test
    public void execute() throws Exception {
        algo.configure(digitalDisplayList, captor);

        algo.execute();
        Mockito.verify(captor, Mockito.times(1)).unlock();

        algo.execute();
        Mockito.verify(captor, Mockito.times(2)).unlock();
    }

    @Test(expected = NullPointerException.class)
    public void executeBeforeConfigure() throws Exception {
        SequentialDiffusion sequentialDiffusion = new SequentialDiffusion();
        sequentialDiffusion.execute();
    }

    @Test
    public void call() throws Exception {
        algo.call(Mockito.mock(DigitalDisplayAsync.class));
    }

}
package captor;

import captor.strategies.TimeStamp;
import digitalDisplay.DigitalDisplay;
import digitalDisplay.DigitalDisplayAsync;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Timer;

/**
 * CaptorImplTest
 * Created by quentin on 20/01/17.
 */
public class CaptorImplTest {

    private static final int PERIOD = 100;
    private static final int EPSILON = 2;

    @InjectMocks
    private CaptorImpl captor;

    @Mock
    private AlgoDiffusion algo;

    @Mock
    private ArrayList<DigitalDisplayAsync> digitalDisplayList;

    @Mock
    private Timer clock;

    @Before
    public void setUp() throws Exception {
        AlgoDiffusion algo = Mockito.mock(AlgoDiffusion.class);
        this.captor = Mockito.spy(new CaptorImpl(algo, PERIOD));
        MockitoAnnotations.initMocks(this);

        Assert.assertNotNull(this.algo);
        Assert.assertNotNull(this.clock);
    }

    @Test(expected = NullPointerException.class)
    public void captorImplWithNullAlgo() throws Exception {
        new CaptorImpl(null, PERIOD);
    }

    @Test(expected = Exception.class)
    public void captorImplWithNegativePeriod() throws Exception {
        new CaptorImpl(Mockito.mock(AlgoDiffusion.class), -1);
    }

    @Test
    public void setAlgo() throws Exception {
        this.captor.setAlgo(Mockito.mock(AlgoDiffusion.class));
    }

    @Test(expected = NullPointerException.class)
    public void setAlgoWithNullAlgo() throws Exception {
        this.captor.setAlgo(null);
    }

    @Test
    public void addDigitalDisplay() throws Exception {
        DigitalDisplayAsync digitalDisplay = Mockito.mock(DigitalDisplayAsync.class);
        this.captor.addDigitalDisplay(digitalDisplay);
    }

    @Test(expected = NullPointerException.class)
    public void addDigitalDisplayWithNullDigitalDisplay() throws Exception {
        this.captor.addDigitalDisplay(null);
    }

    @Test
    public void removeDigitalDisplay() throws Exception {
        DigitalDisplayAsync digitalDisplay = Mockito.mock(DigitalDisplayAsync.class);
        this.captor.addDigitalDisplay(digitalDisplay);
        this.captor.removeDigitalDisplay(digitalDisplay);
    }

    @Test(expected = NullPointerException.class)
    public void removeDigitalDisplayWithNullDigitalDisplay() throws Exception {
        this.captor.removeDigitalDisplay(null);
    }

    @Test
    public void tick() throws Exception {
        this.captor.tick();

        Mockito.verify(this.algo).configure(Matchers.any(), Matchers.any());
        Mockito.verify(this.algo).execute();

        TimeStamp value = this.captor.getValue(Mockito.mock(DigitalDisplayAsync.class));
        Assert.assertEquals(1, (int)value.getValue());
    }

    @Test
    public void start() throws Exception {
        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(0)).tick();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(0)).tick();

        this.captor.start();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(1)).tick();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(2)).tick();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(3)).tick();

        this.captor.stop();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(3)).tick();

        Thread.sleep(PERIOD + EPSILON);
        Mockito.verify(this.captor, Mockito.times(3)).tick();
    }

    @Test
    public void stop() throws Exception {
        this.captor.start();
        this.captor.stop();

        Thread.sleep(PERIOD);
        Mockito.verify(this.captor, Mockito.times(0)).tick();

        Thread.sleep(PERIOD);
        Mockito.verify(this.captor, Mockito.times(0)).tick();
    }

    @Test
    public void getValue() throws Exception {
        Integer value = 0;

        Assert.assertEquals(value, (Integer)captor.getValue(Mockito.mock(DigitalDisplayAsync.class)).getValue());
    }

    @Test(expected = NullPointerException.class)
    public void getValueWithNullDigitalDisplay() throws Exception {
        this.captor.getValue(null);
    }

    @Test
    public void lock() throws Exception {
        this.captor.lock();
    }

    @Test
    public void unlock() throws Exception {
        this.captor.unlock();
    }

    @Test
    public void setTimeStamp() throws Exception {
        this.captor.setTimeStamp(false);

        TimeStamp timeStamp = this.captor.getValue(Mockito.mock(DigitalDisplayAsync.class));

        Assert.assertNull(timeStamp.getTimeStamp());

        this.captor.setTimeStamp(true);

        timeStamp = this.captor.getValue(Mockito.mock(DigitalDisplayAsync.class));

        Assert.assertNotNull(timeStamp.getTimeStamp());
    }

}